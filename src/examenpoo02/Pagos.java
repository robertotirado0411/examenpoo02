/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo02;

/**
 *
 * @author Edgar Guerrero
 */
public class Pagos {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private int horas;
    private Bono hijos;

    public Pagos() {
    }
    
    public Pagos(Pagos otro){
        this.numDocente=otro.numDocente;
        this.nombre=otro.nombre;
        this.domicilio=otro.domicilio;
        this.nivel=otro.nivel;
        this.pagoBase=otro.pagoBase;
        this.horas=otro.horas;
        
    }    
    public Pagos(int numDocente, String nombre, String domicilio, int nivel, float pagoBase, int horas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.horas = horas;
    }
    


 public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoHora() {
        return pagoBase;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoBase = pagoHora;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }
    
    
    public float calcularPago(){
        float pago = 0.0f;
        switch(this.nivel){
            case 0: pago = (this.pagoBase * 1.3f)*this.horas; break;
            case 1: pago = (this.pagoBase * 1.5f)*this.horas; break;
            case 2: pago = (this.pagoBase * 2f)*this.horas; break;
        }
        return pago;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularPago()*.16f;
        return impuesto;
    }    
    
    public float calcularBono(int hijos) {
        float bono=0.0f;
          if(hijos>0 && hijos<3) bono = this.calcularPago()*0.05f;
          if(hijos>2 && hijos<6) bono = this.calcularPago()*0.1f;
          if(hijos>5) bono = this.calcularPago()*0.2f;
        return bono;
    }
    
    public float calcularTotalPagar(int hijos) {
        float totalPagar;
        totalPagar = this.calcularPago() - this.calcularImpuesto() + this.calcularBono(hijos);
        return totalPagar;
    }    
    
public void imprimirPago() {
    System.out.println("Número de docente: " + numDocente);
    System.out.println("Nombre: " + nombre);
    System.out.println("Nivel: " + nivel);
    System.out.println("Pago base: " + pagoBase);
    System.out.println("Horas trabajadas: " + horas);
    System.out.println("Pago total: " + calcularPago());
    System.out.println("Impuesto: " + calcularImpuesto());
    System.out.println("Bono: " + calcularBono(horas));
    System.out.println("Salario total: " + calcularTotalPagar(horas));
}    
}

    